'use strict';
var path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const ebbController = require('./ebbcontroller');
const afController = require('./afcontroller');
const mob = require('./xdm');
const perk = require('./perk');

app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'pug');
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//Buy Button
app.get('/ebb', ebbController.ebb);
app.get('/pidcb', ebbController.pidcb);
app.post('/ebb1', ebbController.ebb1);
app.post('/cart_confirm', ebbController.cart_confirm);
app.post('/cart_update', ebbController.cart_update);

// CDI Auth
app.get('/mob/cb', mob.cb);
app.get('/mob/dummycb', mob.dummycb);
app.get('/mob', mob.mob);

// Perks
app.get('/perk', perk.perk);
// app.get('/perk/callback', myperk.do_perkcallback);

// Autofill
app.get('/af', afController.af);
app.get('/afcb', afController.afcb);
app.get('/afphone', afController.getPhoneAttr);

module.exports = app;


