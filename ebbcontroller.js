// const express = require('express');
// var router = express.Router();
'use strict';

const querystring = require('querystring');

const danal = require('./helpers/danal');
const cartmgr = require('./helpers/cartmgr');
const datamodel = require('./helpers/datamodel');
const session = require('./helpers/chbasesession');
const asyncmgr = require('./helpers/asyncmgr');
const l = require('./helpers/logging');

l.logger.info('Starting....');
l.logger.error('error logging on');
l.logger.warn('warning logging on');
l.logger.debug('debug logging on');


module.exports.ebb = function ebb(req, res, next) {

    if (req.query && req.query.item_url == undefined || req.query.redirect == undefined) {
        res.status(400).send('Bad Request');

    }
    else {

        var session_key = session.getNewSessionKey();

        var my_host_url = req.protocol + '://' + req.get('host');
        var my_redir_url = my_host_url + '/pidcb'; // + '?' + 'sid=' + session_key;

        // var my_redir_url = 'https://r2jboxvgc9.execute-api.us-east-1.amazonaws.com/bbtest'+ '/pidcb' + '?' + 'sid=' + session_key;


        var d = new Date();
        var epoc_time = d.getTime();


        // add session_key, item_url, redir, phone_id_time and akey to db
        var phone_id = new datamodel.PhoneID(null, epoc_time, req.headers, req.query, my_host_url);
        var consent = new datamodel.Consent(req.query.redirect);
        var cart = new datamodel.Cart(req.query.item_url);

        session.createSession(session_key, phone_id, consent, cart, function (err) {

            if (err === null) {
                // l.logger.info("db success, redirecting to " + parsedResponse.EVURL);

                var cdi_auth_redirect_url = 'https://bbdemo.vmcomlab.com/auth/start/';


                var url_parameters = 'state=' + session_key + '&cb=' + my_redir_url;

                l.logger.info('start_cdi_auth: url_parameters=%s', url_parameters);

                res.redirect(cdi_auth_redirect_url + '?' + url_parameters);

                // res.redirect(parsedResponse.EVURL);
            }
            else {

                l.logger.info('db failed!!!');
                res.status(500).send('Error db error createSession failed!');
            }
        });


        // res.render('index', {title: 'ebb - good'});
    }

};


// Callback function from danal redirect
module.exports.pidcb = function pidcb(req, res, next) {
    var cart, phone_id, consent;

    if (req.query && req.query.state == undefined) {
        // he didn't send in the session id
        res.status(400).send('Bad Request');
        return;
    }

    // get the session
    var session_key = req.query.state;
    session.getSession(session_key, function (err, sess) {
        const host_url = req.protocol + '://' + req.get('host');
        if (!req.query.id) {
            // phone id failed do non-prefill cart
        }
        else {
            l.logger.info('Phoneid success!');
            sess.phone_id.mdn = req.query.id;

            danal.registerPhoneIdentification(sess.phone_id.mdn, req.connection.remoteAddress,
                (err, response) => {
                    if (err) {
                        l.logger.error('regPhoneID failed (todo: do a non prefill)')
                    }
                    else {

                        sess.phone_id.akey = response.AuthenticationKey;

                        session.updatePhoneId(session_key, sess, (err) => {
                            if (err) {
                                l.logger.error('updateConsumerPhoneId failed');
                            }
                            else {
                                var action_url = host_url + '/ebb1/' + '&' + 'sessionid=' + req.query.state;
                                res.redirect(sess.consent.consent_url + '?' + 'action_url=' + action_url);
                            }
                        });
                    }
                });
        }
    });
};


// Callback when consent form is posted
module.exports.ebb1 = function ebb1(req, res, next) {
    l.logger.info('ebb1 here bro!');

    if (!(req.query && req.body)) {
        res.status(400).send('Bad Request');
    }

    asyncmgr.doebb1(req.body.sessionid, req.body, function (err, phone_id, response) {
        // response.body.url is the tt cart url
        var myreq = req;
        if (!err) {
            if (phone_id.query.callback == undefined) {
                l.logger.info('ebb1: doing a prefill cart');
                res.render('checkout_id', {cart_url: response.body.url});
            }

            else {
                l.logger.info('ebb1: doing a prefill cart and jsonp callback = ' + phone_id.query.callback);
                res.set('Content-Type', 'text/javascript');
                res.send(phone_id.query.callback + '(' + JSON.stringify({type: 'cart', url: response.body.url}) + ')');
            }
        }
        else {

            if (err.name == 'DANALERR' && response.ErrorCode == '-5036') { // bad zipcode)
                var action_url = myreq.protocol + '://' + myreq.get('host') + '/ebb1/' + '&' + 'sessionid=' + myreq.body.sessionid;
                res.redirect(phone_id.query.redirect + '?' + 'action_url=' + action_url);
            }
            else if (err.name == 'ZIPEXCEED') {
                l.logger.error('ebb1: zipcode retry count exceeded');
                res.render('index', {title: 'too many wrong zipcodes bro!'});

            }

        }

    });

};


// Cart confirms from twotap for purchase and payment
module.exports.cart_confirm = function confirm_cart(req, res, next) {
    l.logger.info('cart confirm here bro!');

    cartmgr.doCartConfirm(req.body, function (err, data) {


        res.json({});

    });


};

module.exports.cart_update = function cart_update(req, res, next) {
    l.logger.info('cart update here bro!');

    cartmgr.doCartUpdate(req.body, function (err, data) {
        res.json({});

    });


};

