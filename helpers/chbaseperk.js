/**
 * Created by taranro on 5/9/17.
 */

/**
 * Created by taranro on 11/15/16.
 */
/*

 couchbase version of perks db interface

 */
"use strict";

var uuid = require('node-uuid');
var l = require('../helpers/logging');

var couchbase = require('couchbase');
var myCluster = new couchbase.Cluster('couchbase://bbdev4.vmcomlab.com');
var myBucket = null;


module.exports.openDB = function openDB() {

    return new Promise(function(resolve, reject) {

        if (myBucket)
        {
            resolve(myBucket);
        }
        else {
            myBucket = myCluster.openBucket('mobsvc2', 'verizon123456', function(err) {
                if (err) {
                    // Failed to make a connection to the Couchbase cluster.
                    reject(err);
                }
                else {
                    resolve(myBucket);
                }
            });
        }

    });



};

module.exports.getCampaign = function getCampaign(campid) {

    return new Promise(function(resolve, reject) {

        myBucket.get(campid, (err, response) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(response.value);
            }

        });
    });

};

module.exports.putRedeemResult = function putRedeemResult(result) {

    return new Promise(function(resolve, reject) {
        let redeem_key = uuid();

        myBucket.insert(redeem_key, {"doc_type" : "Redeem Response", result}, (err, response) => {
            if (err) {
                l.logger.error('putRedeemResult insert failed: ' + JSON.stringify(err));
                reject(err);
            }
            else
            {
                resolve(response);
            }

        });
    });
};
