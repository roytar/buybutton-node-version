/**
 * Created by taranro on 11/15/16.
 */


var uuid = require('node-uuid');
var AWS = require('aws-sdk');
var doc = require('dynamodb-doc');
var crypto = require('crypto');

var l = require('../helpers/logging');
var cartmgr = require('../helpers/cartmgr');
var datamodel = require('../helpers/datamodel');

AWS.config.loadFromPath('./helpers/dbconfig.json');
var dynamoDB = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();


const tableName = "bb_sessions";

"use strict";


module.exports.getNewSessionKey = function getNewSessionKey() {
    var len = 64;

    l.logger.info('new session key');

    return crypto.randomBytes(len).toString('hex');
};


module.exports.createSession = function createSession(session_key, phone_id, consent, cart, fn) {


    var params = {
        TableName: tableName,
        Item: {
            "session_key": session_key,
            "session_create_time": (new Date()).toLocaleString(),

            "info": {
                "phone_id": phone_id,
                "consent": consent,
                "cart": cart

            }
        }
    };


    docClient.put(params, function (err, data) {
        if (err) {
            l.logger.error('createSession failed. ' + err.name + ': ' + err.message);
            return fn(err);
        }
        else {
            // l.logger.info('createSession ret fr docClient params= ' + JSON.stringify(params));
            return fn(null);
        }
    });


};


module.exports.getSession = function getSession(session_key, fn) {
    docClient.get({
        TableName: tableName,
        Key: {
            "session_key": session_key
        }

    }, function (err, data) {
        if (err) {
            l.logger.error("getSession failed. Error: " + err.name + ': ' + err.message);
            return fn(err);
        }
        else {
            if ('Item' in data) {

                fn(null, data.Item.info.phone_id, data.Item.info.consent, data.Item.info.cart);
            } else {
                fn(null, null); // Session not found
            }
        }
    });
};


// todo: generalize the following update functions

module.exports.updateConsent = function updateConsent(session_key, consent, fn) {

    var params = {
        TableName: tableName,
        Key: {
            "session_key": session_key
        },
        UpdateExpression: "set info.consent.form_response = :form_response",
        ExpressionAttributeValues: {

            ":form_response": consent.form_response
        },
        ReturnValues: "UPDATED_NEW"
    };

    l.logger.debug("Updating the consent item...parms = " + JSON.stringify(params, null, 2));
    docClient.update(params, function (err, data) {
        if (err) {
            l.logger.error("updateConsent : Unable to update item. Error: " + err.name + ': ' + err.message);
        } else {
            l.logger.debug("updateConsent UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
        fn(err);
    });


};

module.exports.updateConsumerInfo = function updateConsumerInfo(session_key, consumer_info, fn) {

    var params = {
        TableName: tableName,
        Key: {
            "session_key": session_key
        },
        UpdateExpression: "set info.phone_id.consumer_info = :consumer_info",
        ExpressionAttributeValues: {

            ":consumer_info": consumer_info
        },
        ReturnValues: "UPDATED_NEW"
    };

    l.logger.debug("updateConsumerInfo Updating the consumer_info item..." + JSON.stringify(params, null, 2));
    docClient.update(params, function (err, data) {
        if (err) {
            l.logger.error("updateConsumerInfo Unable to update item. Error: ", +err.name + ': ' + err.message);
        }
        else {
            l.logger.debug("updateConsumerInfo UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
        fn(err);
    });


};

module.exports.updateCart = function updateCart(session_key, cart, fn) {

    var params = {
        TableName: tableName,
        Key: {
            "session_key": session_key
        },
        UpdateExpression: "set info.cart = :cart",
        ExpressionAttributeValues: {

            ":cart": cart
        },
        ReturnValues: "UPDATED_NEW"
    };

    l.logger.debug("updateCart Updating the consumer_info item..." + JSON.stringify(params, null, 2));
    docClient.update(params, function (err, data) {
        if (err) {
            l.logger.error("updateCart Unable to update item. Error: ", +err.name + ': ' + err.message);
        }
        else {
            l.logger.debug("updateCart UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
        fn(err, data);
    });


};