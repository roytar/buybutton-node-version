/**
 * Created by taranro on 11/21/16.
 */
// TwoTap specific
"use strict";

var waterfall = require('async-waterfall');


var request = require('request');
// require('request-debug')(request);

var uuid = require('node-uuid');

const l = require('../helpers/logging');
var danal = require('../helpers/danal');
var datamodel = require('../helpers/datamodel');
var session = require('../helpers/chbasesession');


// todo: put the following in a json config file
var tt_custom_css_url = 'https://secure-ads.pictela.net/rm/ads/212136/c/integration_twotap.css';
var tt_prepare_checkout_url = 'https://checkout.twotap.com/prepare_checkout';
var tt_wallet_meta_url = 'https://api.twotap.com/v1.0/wallet/meta';
var tt_public_token = 'f2e7e593b8027937a867fc30c0f327';
var tt_private_token = 'd6dfbc18d8f7f7789a06cdc6e7a12a56ba0f7953c0cdc4c16f';
var tt_test_mode = 'fake_confirm'; //  # Optional) fake_confirm. A way to test the interface without making actual purchases.
//# Wallet:
//    # Send 'default_off' if you want to set the 'Save checkout info for future use' toggle as off by default.
//# Send 'off' to disable the wallet completely.
var tt_wallet = 'off';

function getNextCorrelationID() {
    var uuid1 = uuid.v1();
    return uuid1.replace(/-/g, '');
}
module.exports.sendCartMeta = function sendCartMeta(danal_key, consumer_info, consumer_payment_info, fn) {

    var pci_zone = true;

    var tt_consumer_info = {};
    var tt_consumer_payment_info = {};

    translate_ci(tt_consumer_info, consumer_info);

    translate_ci_payment(tt_consumer_payment_info, tt_consumer_info, consumer_payment_info,
        consumer_info);


    // if not in pc zone delete the cc stuff
    if (!pci_zone) {
        delete tt_consumer_payment_info.card_number;
        delete tt_consumer_payment_info.expiry_date_month;
        delete tt_consumer_payment_info.expiry_date_year;

    }

    var payload = {'field_type': 'payment', 'meta_fields': tt_consumer_payment_info, 'expires_in': 3600};

    payload.meta_fields.danal_payment_id = consumer_payment_info.PaymentId;
    if (!pci_zone) {
        payload.meta_fields.danal_key = sess.phone_id.akey;
    }

    l.logger.info('sending meta: ', JSON.stringify(payload,null,'\t'));

    request.post({
            uri: tt_wallet_meta_url + '?public_token=' + tt_public_token,
            json: true,
            headers: {"content-type": "application/json", 'User-Agent': 'vzw2.0'},
            body: payload,
            time: true, timeout: 15000
        },

        function (err, response, body) {
            if (err) {
                // request err, http?
                l.logger.error('sendCartMeta request error: ' + err.name + ': ' + err.message);
                // callback(error, response.statusCode, response.statusMessage);
                fn(err, response);
            }
            else if (response.statusCode == 200) {

                l.logger.debug('sendCartMeta send meta good http response = ' + JSON.stringify(response), null, '\t');
                if (response.body.message == 'done') {
                    tt_consumer_info.payment_meta_id = response.body.meta_id;

                    l.logger.info('sendCartMeta meta_id = ' + response.body.meta_id);
                    fn(null, tt_consumer_info);

                } else {
                    l.logger.error('sendCartMeta bad response = ' + response.body.message + ': ' +
                    response.body.description);
                    l.logger.debug('sendCartMeta: ', JSON.stringify(response), null, '\t');
                    fn(666, response);
                }

            }
            else {
                l.logger.error('sendCartMeta bad http response = ' + JSON.stringify(response));
                err = new Error;
                err.name = 'META';
                err.message = 'bad http response from send meta: ' + JSON.stringify(response);
                fn(err, response);
            }


        });

};

module.exports.getCartURL = function getCartURL(session_key, sess, tt_consumer_info, fn) {
    var confirm = {
        'method': 'sms',
        'sms_confirm_url':sess.phone_id.host_url + '/cart_confirm',
        'sms_update_url': sess.phone_id.host_url + '/cart_update'
    };
    var close_button = {'show': 'true'};

    var checkoutRequest = {};
    checkoutRequest['products'] = [{url: sess.phone_id.query.item_url}];
    checkoutRequest['public_token'] = tt_public_token;
    checkoutRequest['confirm'] = confirm;
    checkoutRequest['custom_css_url'] = tt_custom_css_url;
    checkoutRequest['unique_token'] = getNextCorrelationID();


    if (tt_consumer_info !== undefined)
        checkoutRequest['input_fields'] = tt_consumer_info;
    else
        l.logger.warning('getGartURL: no consumer info, tt_consumer_info is null.  No pre-fill!');

    checkoutRequest['test_mode'] = tt_test_mode;
    checkoutRequest['wallet'] = tt_wallet;
    checkoutRequest['close_button'] = close_button;
    // checkoutRequest['top_banner'] = top_banner;
    checkoutRequest['notes'] = session_key;
    var myphone_id = sess.phone_id;
    var d = new Date();
    var launch_time = d.getTime();

    sess.cart = new datamodel.Cart(sess.phone_id.query.item_url, 'blank', checkoutRequest.unique_token, launch_time);

    l.logger.debug('checkoutRequest: ', JSON.stringify(checkoutRequest, null, '\t'));

    request.post({
            uri: tt_prepare_checkout_url,
            json: true,
            headers: {"content-type": "application/json", 'User-Agent': 'vzw2.0'},
            body: {checkout_request: checkoutRequest},
            time: true, timeout: 15000
        },

        function (err, response, body) {
            if (err) {
                // request error, http?
                l.logger.error('getCartURL request error: ' + err.name + ': ' + err.message);
                fn(err, response);
            }
            else if (response.statusCode == 200) {

                // l.logger.debug('getCartURL good response form tt = ' + JSON.stringify(response));
                if (response.body.message == 'done') {

                    sess.cart.url = response.body.url;

                    // update the cart in the db with url and unique id
                    session.updateCart(session_key, sess, function (err, data) {
                        if (!err) {
                            l.logger.info("getCartURL update cart success!!");

                            if (myphone_id.query.callback !== undefined) {
                                l.logger.info('jsonp');
                                response.body.jsonp_url = sess.phone_id.query.callback + '(' + JSON.stringify({
                                        type: 'cart',
                                        url: response.body.url
                                    }) + ')';

                            }

                        }
                        else {
                            l.logger.error("getCartURL update cart failed " + err.name + ': ' + err.message);
                        }
                        //fn(err, data);


                    }); // end updateCart

                    fn(null, response);
                }

            }
            else {
                l.logger.error('getCartURL bad http response = ' + JSON.stringify(response));
                err = new Error;
                err.name = 'CART';
                err.message = 'getCartURL response from send meta: ' + JSON.stringify(response);
                fn(err, response);
            }
        }); // end post


};

function translate_ci(tt_consumer_info, consumer_info) {

    try {

        if (member_and_not_none(consumer_info, 'ConsumerEmail'))
            tt_consumer_info['email'] = consumer_info['ConsumerEmail'];

        tt_consumer_info['billing_title'] = 'Mr.';

        if (member_and_not_none(consumer_info, 'ConsumerFirstName'))
            tt_consumer_info['billing_first_name'] = tt_consumer_info['shipping_first_name'] = consumer_info['ConsumerFirstName'];

        if (member_and_not_none(consumer_info, 'ConsumerLastName'))
            tt_consumer_info['billing_last_name'] = tt_consumer_info['shipping_last_name'] = consumer_info['ConsumerLastName'];

        if (member_and_not_none(consumer_info, 'ConsumerAddress1'))
            tt_consumer_info['billing_address'] = tt_consumer_info['shipping_address'] = consumer_info['ConsumerAddress1'];

        if (member_and_not_none(consumer_info, 'ConsumerCity'))
            tt_consumer_info['billing_city'] = tt_consumer_info['shipping_city'] = consumer_info['ConsumerCity'];

        if (member_and_not_none(consumer_info, 'ConsumerState'))
            tt_consumer_info['billing_state'] = tt_consumer_info['shipping_state'] = state_map(consumer_info['ConsumerState']);

        tt_consumer_info['billing_country'] = tt_consumer_info['shipping_country'] = 'United States of America';

        if (member_and_not_none(consumer_info, 'ConsumerPostalCode'))
            tt_consumer_info['billing_zip'] = tt_consumer_info['shipping_zip'] = consumer_info['ConsumerPostalCode'];

        if (member_and_not_none(consumer_info, 'ConsumerMDN'))
            tt_consumer_info['billing_telephone'] = tt_consumer_info['shipping_telephone'] = consumer_info['ConsumerMDN'].slice(-10);

    }
    catch (err) {
        l.logger.error('translate_ci exception');
        tt_consumer_info = {};
    }
}


function translate_ci_payment(tt_consumer_payment_info, tt_consumer_info, consumer_payment_info, consumer_info) {

    try {


        tt_consumer_payment_info['card_type'] = consumer_payment_info['PaymentType'];

        if (tt_consumer_payment_info['card_type'] == 'Amex')
            tt_consumer_payment_info['card_type'] = 'American Express';

        else if (tt_consumer_payment_info['card_type'] == 'Master')
            tt_consumer_payment_info['card_type'] = 'MasterCard';
        else if (tt_consumer_payment_info['card_type'] == 'Visa')
            tt_consumer_payment_info['card_type'] = 'Visa';
        else if (tt_consumer_payment_info['card_type'] == 'Discover')
            tt_consumer_payment_info['card_type'] = 'Discover';

        tt_consumer_payment_info['card_number'] = consumer_payment_info['Number'];

        tt_consumer_payment_info['card_name'] = consumer_info['ConsumerFirstName'] + ' ' + consumer_info['ConsumerLastName'];

        tt_consumer_payment_info['expiry_date_month'] = '' + (consumer_payment_info['ExpireMonth'] < 10 ? ('0' + consumer_payment_info['ExpireMonth']) :
            consumer_payment_info['ExpireMonth']);

        tt_consumer_payment_info['expiry_date_year'] = consumer_payment_info['ExpireYear'].toString();

        tt_consumer_payment_info['billing_first_name'] = tt_consumer_info['billing_first_name'];

        tt_consumer_payment_info['billing_last_name'] = tt_consumer_info['billing_last_name'];

        tt_consumer_payment_info['billing_address'] = tt_consumer_info['billing_address'];


        tt_consumer_payment_info['billing_city'] = tt_consumer_info['billing_city'];

        tt_consumer_payment_info['billing_state'] = tt_consumer_info['billing_state'];

        tt_consumer_payment_info['billing_country'] = tt_consumer_info['billing_country'];

        tt_consumer_payment_info['billing_zip'] = tt_consumer_info['billing_zip'];

        tt_consumer_payment_info['billing_telephone'] = tt_consumer_info['billing_telephone'];

        tt_consumer_payment_info['billing_title'] = tt_consumer_info['billing_title'];

        tt_consumer_info['payment_meta_description'] =
            tt_consumer_payment_info['card_type'] + ' ending in ' +
            tt_consumer_payment_info['card_number'].slice(-3);
    }
    catch (err) {
        l.logger.error('translate_ci_payment exception');
    }
}


function member_and_not_none(obj, k) {

    if (obj[k] != null)
        return true;
    else
        return false;
}


function state_map(abv) {
    if (abv.length > 2)
        return abv;

    var states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
    }
    var t;
    var ab;

    try {

        t = abv.replace('.', '');
        ab = states[t.toUpperCase()];
    }
    catch (err) {
        l.logger.error('state_map: Exception - ' + abv.toUpperCase() + ' not in states');
        ab = '';
    }

    return ab;
}


module.exports.doCartConfirm = function doCartConfirm(body, fn) {

    if (body.notes == undefined) {
        l.logger.error('doCartConfirm: no session_key (should be in body.notes)!')
        fn(1, null);
        return;
    }
    l.logger.info('doCartConfirm');
    var session_key = body.notes;

    var callPath = '/v1.0/purchase/confirm?private_token=' + tt_private_token;

    var testMode = body.test_mode;
    var purchaseId = body.purchase_id;
    var uniqueToken = body.unique_token;
    var sites = body.sites;
    var message = body.message;

    // get the session

    session.getSession(session_key, function (err, sess) {
        if (err) {
            l.logger.error("cartConfirm: getSession failed " + err.name + ': ' + err.message);

            var err = new Error;
            err.name = 'NOSESS';
            err.message = 'Session not found';
            fn(err, null);
            return;

        }
        else { // got the session phone_id, consent and cart are set.
            l.logger.info("cartConfirm: getSession success.");

            // now check if my unique id and item_url matches with tt

            if (uniqueToken == sess.cart.unique_token) {

                // ok confirm with tt
                callPath += '&test_mode=' + testMode;
                request.post({
                        uri: 'https://api.twotap.com' + callPath,
                        json: {'purchase_id': purchaseId}
                    },

                    function (err, response, body) {
                        l.logger.info('cart_confirm: tt response status: ' + JSON.stringify(response));

                        var d = new Date();
                        var epoc_time = d.getTime();

                        sess.cart.confirm_info = {state: 1, timestamp: epoc_time, purchaseId: purchaseId, message: message};

                        session.updateCart(session_key, sess, function (err, data) {
                            if (!err) {
                                l.logger.info("doCartConfirm update cart success!!");

                            }
                            else {
                                l.logger.error("doCartConfirm update cart failed " + err.name + ': ' + err.message);
                            }
                            fn(err, data);


                        });


                    }); // end post

            }

            else {  // token didn't match!

                l.logger.error("cartConfirm: token didn't match!");
                var err = new Error;
                err.name = 'BADORD';
                err.message = 'Bad order';
                fn(err, null);

            }


        } // end get session success
    }); // end get session

};

module.exports.doCartUpdate = function doCartUpdate(body, fn) {
    l.logger.info('doCartUpdate');

    if (body.notes == undefined) {
        l.logger.error('doCartUpdate: no session_key (should be in body.notes)!')
        fn(1, null);
        return;
    }
    l.logger.info('doCartUpdate');
    var session_key = body.notes;

    var uniqueToken = body.unique_token;
    var sites = body.sites;
    var message = body.message; // should be 'done'
    var final_message = body.final_message; // should be 'done'

    session.getSession(session_key, function (err, sess) {
        if (err) {
            l.logger.error("cartUpdate: getSession failed " + err.name + ': ' + err.message);

            var err = new Error;
            err.name = 'NOSESS';
            err.message = 'Session not found';
            fn(err, null);
            return;

        }
        else { // got the session phone_id, consent and cart are set.
            l.logger.info("cartUpdate: getSession success.");

            // now check if my unique id and item_url matches with tt

            if (uniqueToken == sess.cart.unique_token) {


                var d = new Date();
                var epoc_time = d.getTime();

                sess.cart.update_info = {state: 1, timestamp: epoc_time, message: message, final_message: final_message};


                session.updateCart(session_key, sess, function (err, data) {
                    if (!err) {
                        l.logger.info("doCartUpdate update cart success!!");

                    }
                    else {
                        l.logger.error("doCartUpdate update cart failed " + err.name + ': ' + err.message);
                    }
                    fn(err, data);


                });


            }


            else {  // token didn't match!

                l.logger.error("cartUpdate: token didn't match!");
                var err = new Error;
                err.name = 'BADORD';
                err.message = 'Bad order';
                fn(err, null);

            }


        } // end get session success
    }); // end get session

};

