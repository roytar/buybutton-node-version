/**
* Created by taranro on 12/5/16.
*/

'use strict';
const winston = require('winston');
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV || 'development';
const logDir = 'log';
// Create the log directory if it does not exist
// if (!fs.existsSync(logDir)) {
//     fs.mkdirSync(logDir);
// }
// const tsFormat = function () {
//     return (new Date()).toLocaleString()
// };
// var logfile = path.basename(process.argv[1]) + '.log';

const tsFormat = () => (new Date()).toLocaleTimeString();

const logger = new (winston.Logger)({
    transports: [

        new (winston.transports.Console)({
            timestamp: tsFormat,
            level: 'info',
            json: false
        })//,
        // new (winston.transports.File)({
        //     filename: logDir + '/' + logfile,
        //     timestamp: tsFormat,
        //     level: env === 'development' ? 'debug' : 'info',
        //     json: false
        // })
    ]
});

module.exports.logger = logger;
// logger.info('Hello world');
// logger.warn('Warning message');
// logger.debug('Debugging info');
