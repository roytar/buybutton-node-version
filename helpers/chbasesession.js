/**
 * Created by taranro on 11/15/16.
 */
/*

 couchbase version of simple session manager

 */
"use strict";

var uuid = require('node-uuid');
var l = require('../helpers/logging');

var couchbase = require('couchbase');
var myCluster = new couchbase.Cluster('couchbase://bbdev4.vmcomlab.com');
var myBucket = myCluster.openBucket('mobsvc');



module.exports.getNewSessionKey = function getNewSessionKey() {

    return uuid();
};


module.exports.createSession = function (session_key, phone_id, consent, cart, fn) {



    myBucket.insert(session_key, {doc_type: "bbtrans", "create_time" : new Date(),phone_id, consent, cart},fn);


};


module.exports.getSession = function getSession(session_key, fn) {

    myBucket.get(session_key, (err, response)=> {
        if (!err) {
            fn(err, response.value);
        }
        else {
            fn(err, response);
        }

    });

};

module.exports.updatePhoneId = function updatePhoneId(session_key, sess, fn) {

    myBucket.replace(session_key, sess, (err, response)=> {
        if (!err) {
            fn(err, response.value);
        }
        else {
            fn(err, response);
        }

    });
};

module.exports.updateConsent = function updateConsent(session_key, sess, fn) {

    myBucket.replace(session_key, sess, (err, response)=> {
        if (!err) {
            fn(err, response.value);
        }
        else {
            fn(err, response);
        }

    });


};

module.exports.updateConsumerInfo = function updateConsumerInfo(session_key, sess, fn) {
    fn(null, null);

    // myBucket.replace(session_key, sess, (err, response)=> {
    //     if (!err) {
    //         fn(err, response.value);
    //     }
    //     else {
    //         fn(err, response);
    //     }
    //
    // });

};

module.exports.updateCart = function updateCart(session_key, sess, fn) {

    myBucket.replace(session_key, sess, (err, response)=> {
        if (!err) {
            fn(err, response.value);
        }
        else {
            fn(err, response);
        }

    });

};