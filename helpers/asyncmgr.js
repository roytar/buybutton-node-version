/**
 * Created by taranro on 11/30/16.
 */

"use strict";
var waterfall = require('async-waterfall');
const l = require('../helpers/logging');
var danal = require('../helpers/danal');
var datamodel = require('../helpers/datamodel');
var session = require('../helpers/chbasesession');
var cartmgr = require('../helpers/cartmgr');


module.exports.doebb1 = function doebb1(session_key, form_response, main_fn) {

    waterfall([
        // Get the session
        function (callback) {

            session.getSession(session_key, function (err, sess) {
                if (!err) {
                    l.logger.info("getSession success!!");

                    if (sess.consent.form_response.retry_count >= 3) {
                        var err = new Error;
                        err.name = 'ZIPEXCEED';
                        err.message = 'too many zipcode retries';

                    }
                    else {
                        sess.consent.form_response.retry_count += 1;

                    }

                }
                else {
                    l.logger.error("getSession failed " + err.name + ': ' + err.message);
                }

                callback(err, sess);

            });
        },
        // Update the session with the results of the consent form
        function (sess, callback) {
            // consent.form_response = form_response (works on osx but not ubuntu).
            //  For some reason, dynamodb doesnt like the form_response.
            //    so we've got to copy the values individually to the consent object.

            sess.consent.form_response.zipcode = form_response.zipcode;
            sess.consent.form_response.consentid = form_response.consentid;
            sess.consent.form_response.consent_permission = form_response.consent_permission||' ';
            sess.consent.form_response.date = form_response.date;
            sess.consent.form_response.ppUrl = form_response.pp_Url;
            sess.consent.form_response.tc_url = form_response.tc_url;
            sess.consent.form_response.response = 'responded';



            session.updateConsent(session_key, sess, function (err) {
                if (!err) {
                    l.logger.info("update consent success!!");

                }
                else {
                    l.logger.error("update consent failed " + err.name + ': ' + err.message);
                }
                callback(err, sess);


            });
        },

        // Find out if authorization is required via danal IAR
        function (sess, callback) {
            danal.isAuthRequired(sess.phone_id.akey, function (err, response) {
                if (!err) {
                    l.logger.info('iar response = ' + JSON.stringify(response),null,4);
                    if (response.Carrier !== 'VZW') {
                        err = new Error;
                        err.name = 'NOTVZW';
                        err.message = 'Carrier ' + response.Carrier + 'is not supported';
                    }
                    else if (response.DataSourceCategory !== '1000' && response.DataSourceCategory !== '1001') {
                        err = new Error;
                        err.name = 'NODATA';
                        err.message = 'Data source category (' + response.DataSourceCategory + ')' +
                            ' not sourced by carrier';
                    }

                }
                else {
                    l.logger.error('iar Error: ' + err.name + ': ' + err.message);
                }
                callback(err, sess);
            });
        },

        // Get the Consumer Info, danal GCI
        function (sess, callback) {
            danal.getConsumerInfo(sess.phone_id.akey, sess.consent,
                function (err, response) {
                    if (!err) {
                        l.logger.debug('gci response = ' + JSON.stringify(response),null, '\t');

                        // phone_id.consumer_info = response;
                        // need to copy the response to consumer_info
                        //  otherwise hasOwnProperty doesn't work!

                        // sess.phone_id.consumer_info = JSON.parse(JSON.stringify(response)); // simplistic shallow obj copy.
                        callback(null, sess, response);

                    }
                    else {

                        l.logger.error('gci Error: ' + err.name + ': ' + err.message);
                        callback(err, sess);

                    }


                });
        },


        // Get the payment summary information and the first credit card in the list using danal GCPS
        function (sess, consumer_info, callback) {
            danal.getConsumerPaymentSummary(sess.phone_id.akey, sess.consent, function (err, response) {
                if (!err) {
                    // l.logger.info('gcps repsonse = ' + JSON.stringify(response, null, '\t'));
                    var paymentInfo = JSON.parse(response.PaymentInfo);
                    // l.logger.info('gcps paymentInfo = ' + JSON.stringify(paymentInfo, null, '\t'));

                    // Just get the first credit card in the list
                    var first_cc = null;
                    for (var i = 0; i < paymentInfo.length; i++) {
                        if (paymentInfo[i].AccountType == "CreditCard") {
                            first_cc = paymentInfo[i];
                            break;
                        }
                    }

                    // sess.phone_id.first_cc = first_cc;

                    l.logger.info('mdn = ' + consumer_info.ConsumerMDN + ' first_cc = ', JSON.stringify(first_cc));

                }
                else {
                    l.logger.error('gcps Error: ' + err.name + ': ' + err.message);
                }
                callback(err, sess, first_cc, consumer_info);
            });
        },

        // Get the payment detail information on the first credit card in the list using danal GCPD
        function (sess, first_cc, consumer_info, callback) {
            danal.getConsumerPaymentDetail(sess.phone_id.akey,first_cc.PaymentId, function (err, response) {
                if (!err) {
                    // l.logger.info('gcps repsonse = ' + JSON.stringify(response, null, '\t'));
                    var paymentInfo = JSON.parse(response.PaymentInfo);
                    l.logger.info('gcpd paymentInfo = ' + JSON.stringify(paymentInfo, null, '\t'));
                    var first_cc = paymentInfo[0];
                }
                else {
                    l.logger.error('gcpd Error: ' + err.name + ': ' + err.message);
                }
                callback(err, sess, first_cc, consumer_info);
            });
        },

        // Send the payment info to tt as meta
        function (sess, consumer_payment_info, consumer_info, callback) {

            var tt_consumer_info = {};

            cartmgr.sendCartMeta(sess.phone_id.akey, consumer_info, consumer_payment_info,
                function (err, response) {
                    if (!err) {
                        l.logger.info('sendCartMeta response = ' + JSON.stringify(response, null, '\t'));
                        tt_consumer_info = response;
                    }
                    else {
                        l.logger.error('sendCartMeta Error: ' + +err.name + ': ' + err.message);
                    }

                    callback(err, sess, tt_consumer_info);

                });
        },

        // Get the cart url
        function (sess, tt_consumer_info, callback) {

            cartmgr.getCartURL(session_key, sess, tt_consumer_info,
                function (err, response) {
                    if (!err) {
                        l.logger.info('getCartURL response (good)');
                    }
                    else {
                        l.logger.error('getCartURL Error: ' + err.name + ': ' + err.message);
                    }
                    callback(null, sess, response); // response has cart url

                });
        }


    ], function (err, sess, result) {
        if (err) {

            l.logger.info('waterfall error = ' + err.name + ': ' + err.message);
            main_fn(err, sess.phone_id, result);


        }
        else {
            l.logger.info('main end waterfall ');
            l.logger.info('main cartURL= ' + result);
            main_fn(null, sess.phone_id, result);

        }

    });

};