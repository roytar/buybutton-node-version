/**
 * Created by taranro on 11/28/16.
 */


// My items to be stored in db.  All have constructor of Object because dyndn doc only works with Object
function PhoneID(akey, start_time, headers, query, my_host_url) {
    this.akey = akey;
    this.start_time = start_time;
    this.headers = headers;
    this.query = query;
    this.host_url = my_host_url;
}
PhoneID.prototype = {
    constructor: Object
};

module.exports.PhoneID = PhoneID;

function Consent(consent_url) {
    this.consent_url = consent_url;
    this.form_response = {retry_count: 0};
}
Consent.prototype = {
    constructor: Object
};

module.exports.Consent = Consent;


function Cart(item_url, cart_url, unique_token, launch_time) {
    this.item_url = item_url;
    this.launch_time = launch_time;
    this.confirm_info = {state: 0};
    this.update_info = {state: 0};
    this.unique_token = unique_token;
    this.url = cart_url;
}
Cart.prototype = {
    constructor: Object
};

module.exports.Cart = Cart;

