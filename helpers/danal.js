/**
 * Created by taranro on 11/8/16.
 */
"use strict";
const l = require('../helpers/logging');

var crypto = require('crypto');
var fs = require('fs');
var querystring = require('querystring');

var moment = require('moment');
var forge = require('node-forge');
var NodeRSA = require('node-rsa');
var request = require('request');
var uuid = require('node-uuid');

var config = require('../helpers/config');

var ENCRYPTION_KEY = 'EncryptionKey';

var DECRYPT_PARAM_NAMES = [
    'ConsumerFirstName',
    'ConsumerLastName',
    'ConsumerMDN',
    'ConsumerEmail',
    'ConsumerAddress1',
    'ConsumerAddress2',
    'ConsumerCity',
    'ConsumerState',
    'ConsumerPostalCode',
    'ConsumerCountryCode',
    'PaymentInfo',
    'UserToken',
    'AccountType',
    'Carrier',
    'ServicePlan',
    'AccountStatus',
    'IsPrimaryAccount',
    'AccountRegistrationDate',
    'DeviceVendor',
    'DeviceModel',
    'DeviceID',
    'SMSCapability',
    'MMSCapability',
    'LocationCapability',
    'ConsumerMobileNumber',
    'PhoneAttributes'
];

/* Set keys */
var danalPKCS8 = fs.readFileSync(config.danal.pkcs8_key_path, { encoding: 'binary' });
var danalPKCS8KeyAsn1 = forge.asn1.fromDer(danalPKCS8);
var danalPKCS8PrivateKey = forge.pki.privateKeyFromAsn1(danalPKCS8KeyAsn1);

var danalPEM = fs.readFileSync(config.danal.pem_key_path, { encoding: 'binary' });
var danalPEMKey = new NodeRSA(danalPEM.toString('ascii'), {
    encryptionScheme: {
        scheme: 'pkcs1'
    }
});
/* End of Set keys */


// Utility functions:

function getNextCorrelationID() {
    var uuid1 = uuid.v1();
    return uuid1.replace(/-/g, '');
}


function doRequest(api, fn) {
    var danalRequest = buildRequest(api);
    danalRequest = signRequest(danalRequest);

    request.post({ uri: config.danal.url, headers: { 'User-Agent': 'vzw2.0' }, body: danalRequest, time: true, timeout: 15000 },
        function (err, response, body) {
            if (err) {
                // request error, http?
                l.logger.error('doRequest request error: ' + err.name + ': ' + err.message);
                // callback(error, response.statusCode, response.statusMessage);
                fn(err, response);
            }

            var parsedResponse = parseResponse(body);


            if (parsedResponse.ErrorCode < 0) {
                // danal error
                try {
                    l.logger.info(parsedResponse.Method + ": ErrorCode= " + parsedResponse.ErrorCode + " : " + parsedResponse.ErrorDescription);
                    // var danalError = new Error(parsedResponse.ErrorCode + " : " + parsedResponse.ErrorDescription);
                    throw {
                        name: "DANALERR",
                        message: parsedResponse.ErrorCode + " : " + parsedResponse.ErrorDescription
                    };
                }
                catch
                    (err) {
                    fn(err, parsedResponse);
                }
            }

            else {

                // l.logger.info(parsedResponse.Method + ": ErrorCode= " + parsedResponse.ErrorCode + " response:" +
                //     JSON.stringify(parsedResponse));
                fn(null, parsedResponse);
            }

        });

}

/* Creating the request */
function buildRequest(request) {
    var rawRequest = '';
    var correlationID = getNextCorrelationID();
    var timestamp = moment().utc().format("YYYYMMDDHHmmss");

    for (var key in request) {
        rawRequest += key + "=" + encodeURIComponent(request[key]) + "&";
    }

    rawRequest += "TimeStamp=" + encodeURIComponent(timestamp);
    return rawRequest;
}


function startPhoneID(redirectURL, callback) {
    var danalRequest = null;

    var startPhoneIDReq = {
        Method: 'StartPhoneIdentificationReq',
        Version: '1.0.0',
        MerchantID: config.danal.merchant_id,
        CorrelationID: getNextCorrelationID(),
        RedirectURL: redirectURL
    };


    danalRequest = buildRequest(startPhoneIDReq);
    danalRequest = signRequest(danalRequest);

    doRequest(startPhoneIDReq, callback);
}


function dateToyyyymmddhhmmss(date) {
    function pad2(n) {
        return n < 10 ? '0' + n : n
    }

    return ( date.getFullYear().toString() + pad2(date.getMonth() + 1) + pad2(date.getDate()) + pad2(date.getHours())
    + pad2(date.getMinutes()) + pad2(date.getSeconds()) );
}

function isAuthRequired(akey, callback) {
    var danalRequest = null;
    var d = new Date();

    var isAuthRequired = {
        Method: 'IsAuthRequiredReq',
        Version: '1.0.0',
        MerchantID: config.danal.merchant_id,
        CorrelationID: getNextCorrelationID(),
        AuthenticationKey: akey,
        TimeStamp: dateToyyyymmddhhmmss(d)
    };

    doRequest(isAuthRequired, callback);

}


function getPhoneAttributes(mdn, callback) {
    var danalRequest = null;
    var d = new Date();

    var getPhoneAttributes = {
        Method: 'GetPhoneAttributesReq',
        Version: '1.0.0',
        MerchantID: config.danal.merchant_id,
        CorrelationID: getNextCorrelationID(),
        ConsumerMDN: '+1'+mdn,
        IntendedUseCase: "AR",
        AttributeGroups: "TokenInfo,ServiceInfo,AccountInfo,DeviceInfo",
        TimeStamp: dateToyyyymmddhhmmss(d)
    };

    doRequest(getPhoneAttributes, callback);

}

function registerPhoneIdentification(mdn, ip, callback) {
    var danalRequest = null;
    var d = new Date();

    var registerPhoneIdentification = {
        Method: 'RegisterPhoneIdentificationReq',
        Version: '1.0.0',
        MerchantID: config.danal.merchant_id,
        CorrelationID: getNextCorrelationID(),
        ConsumerMDN: '+1'+mdn,
        ConsumerIP: ip,
        PhoneIDType: "OTP",
        IdDetails: 'phone detail',
        TimeStamp: dateToyyyymmddhhmmss(d)
    };

    doRequest(registerPhoneIdentification, callback);

}


function getConsumerInfo(akey, consent, callback, usecase) {

    var now = new Date();
    var cts = new Date(consent.form_response.date);

    if (usecase == undefined) {
        usecase = 'PC';
    }

    var getConsumerInfo = {};

    if (usecase == 'AR') {
        getConsumerInfo = {
            Method: 'GetConsumerInfoReq',
            Version: '1.0.0',
            MerchantID: config.danal.merchant_id,
            CorrelationID: getNextCorrelationID(),
            AuthenticationKey: akey,
            IntendedUseCase: usecase,
            TimeStamp: dateToyyyymmddhhmmss(now)
        };
    }

    else {
        var getConsumerInfo = {
            Method: 'GetConsumerInfoReq',
            Version: '1.0.0',
            MerchantID: config.danal.merchant_id,
            CorrelationID: getNextCorrelationID(),
            AuthenticationKey: akey,
            IntendedUseCase: usecase,
            ConsentID: consent.form_response.consentid,
            ConsentTimeStamp: dateToyyyymmddhhmmss(cts),
            ConsumerAuth: consent.form_response.zipcode,
            ConsumerAuthType: 'PostalCode',
            TimeStamp: dateToyyyymmddhhmmss(now)
        };


    }

    doRequest(getConsumerInfo, callback);

}

function getConsumerPaymentSummary(akey, consent, callback) {

    /*
     payload = OrderedDict([
     ('Method', 'GetConsumerPaymentSummaryReq'),
     ('Version', '1.0.0'),
     ('MerchantID', myMerchantID),
     ('AuthenticationKey', akey),
     ('IntendedUseCase', 'PC'),
     ('ConsentID', ConsentID),
     ('ConsentTimeStamp', ConsentTimeStamp),
     ('CorrelationID', Correlation),
     ('TimeStamp', time.strftime("%Y%m%d%H%M%S", time.gmtime()))
     ])
     */

    var now = new Date();
    var cts = new Date(consent.form_response.date);

    var GetConsumerPaymentSummary = {
        Method: 'GetConsumerPaymentSummaryReq',
        Version: '1.0.0',
        MerchantID: config.danal.merchant_id,
        CorrelationID: getNextCorrelationID(),
        AuthenticationKey: akey,
        IntendedUseCase: 'PC',
        ConsentID: consent.form_response.consentid,
        ConsentTimeStamp: dateToyyyymmddhhmmss(cts),
        TimeStamp: dateToyyyymmddhhmmss(now)
    };
    doRequest(GetConsumerPaymentSummary, callback);
}

function getConsumerPaymentDetail(akey, PaymentID, callback) {

    var now = new Date();

    var GetConsumerPaymentDetail = {
        Method: 'GetConsumerPaymentDetailsReq',
        Version: '1.0.0',
        MerchantID: config.danal.merchant_id,
        CorrelationID: getNextCorrelationID(),
        AuthenticationKey: akey,
        PaymentID: PaymentID,
        TimeStamp: dateToyyyymmddhhmmss(now)
    };
    doRequest(GetConsumerPaymentDetail, callback);
}

function retrieveCC(danalKey, danalPaymentId, callback) {
    var danalRequest = null;

    danalRequest = buildRequest(danalKey, danalPaymentId);
    danalRequest = signRequest(danalRequest);

    request.post({ uri: config.danal.url, headers: { 'User-Agent': 'Two Tap' }, body: danalRequest, time: true, timeout: 15000 }, function (error, response, body) {
        var parsedResponse = parseResponse(body);

        try {
            var payment = JSON.parse(parsedResponse.PaymentInfo);

            var cc = {};
            cc.expiry_date_month = ('0' + payment[0].ExpireMonth.toString()).slice(-2);
            cc.expiry_date_year  = payment[0].ExpireYear.toString();
            cc.card_number       = payment[0].Number;

            callback(null, cc);
        } catch(err) {
            callback("Couldn't find a Danal credit card.");
        }
    });
}


function signRequest(rawRequest) {
    var md = forge.md.sha1.create();
    md.update(rawRequest, 'utf8');
    var signature = danalPKCS8PrivateKey.sign(md);
    var signature64 = forge.util.encode64(signature);
    var urlEncodedSignature = encodeURIComponent(signature64);
    var signedRequest = rawRequest + "&Sign=" + urlEncodedSignature;
    return signedRequest;
}


/* End of Creating the request */

/* Parsing the response */
function parseResponse(responseBody) {
    var parsedResponse = querystring.parse(responseBody);

    if (parsedResponse[ENCRYPTION_KEY]) {
        parsedResponse = decryptResponse(parsedResponse, parsedResponse[ENCRYPTION_KEY]);
    }

    return parsedResponse;
}

function decryptResponse(parsedResponse, encryptedKeyEncoded) {
    encryptedKeyEncoded = decodeURIComponent(encryptedKeyEncoded);

    // Use the RSA/PKCS1 key to decrypt the encryption key.
    var decryptedKeyBase4 = danalPEMKey.decrypt(encryptedKeyEncoded, 'base64');
    var decryptedKey = danalPEMKey.decrypt(encryptedKeyEncoded);

    for (var key in DECRYPT_PARAM_NAMES) {
        var param = DECRYPT_PARAM_NAMES[key];

        var encryptedValue = parsedResponse[param];

        if (encryptedValue) {
            var decryptedValue = decryptStringCBCorCTR('aes-128-ctr', decodeURIComponent(encryptedValue), decryptedKey, config.danal.salt);
            parsedResponse[param] = decryptedValue;
        }
    }

    return parsedResponse;
}


var decryptStringCBCorCTR = function(encryptionAlg, encryptedString, decryptedKey) {
    var decipher = crypto.createDecipheriv(encryptionAlg, decryptedKey, new Buffer(config.danal.salt));
    decipher.setAutoPadding(true);
    var decryptedData = decipher.update(decodeURIComponent(encryptedString), 'base64', 'utf8');
    decryptedData += decipher.final('utf8');

    return decryptedData;
};
/* End of Parsing the response */

module.exports.retrieveCC = retrieveCC;
module.exports.startPhoneID = startPhoneID;
module.exports.isAuthRequired = isAuthRequired;
module.exports.getPhoneAttributes = getPhoneAttributes;
module.exports.registerPhoneIdentification = registerPhoneIdentification;
module.exports.getConsumerInfo = getConsumerInfo;
module.exports.getConsumerPaymentSummary = getConsumerPaymentSummary;
module.exports.getConsumerPaymentDetail = getConsumerPaymentDetail;


