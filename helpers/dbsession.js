/**
 * Created by taranro on 11/15/16.
 */
/*

 couchdb version of simple session manager

 */
"use strict";

var uuid = require('node-uuid');
var l = require('../helpers/logging');
var request = require('request');
var crypto = require('crypto');
var couchdb = require('../helpers/couchdbconfig');

const dbName = "sessions0";

module.exports.getNewSessionKey = function getNewSessionKey() {
    var len = 64;

    l.logger.info('new session key');

    return crypto.randomBytes(len).toString('hex');
};


module.exports.createSession = function createSession(session_key, data, fn) {

    request.put({
            uri: couchdb.serverURL + '/' + dbName + '/' + session_key,
            json: true,
            headers: {"content-type": "application/json", 'User-Agent': 'vzw2.0'},
            body: data,
            time: true, timeout: 15000
        },

        function (err, response, body) {
            if (err) {
                // request error, http?
                l.logger.error('createSession request error: ' + err.name + ': ' + err.message);
                fn(err, response);
            }
            else if (response.statusCode == 201 && response.body['ok'] == true) {

                l.logger.debug('createSession good response fr couchDB = ' + JSON.stringify(response, 2));


                fn(null, response);


            }
            else {
                l.logger.error('createSession bad http response = ' + JSON.stringify(response, 2));
                err = new Error;
                err.name = 'SES';
                err.message = 'createSession response from send couchDB: ' + JSON.stringify(response, 2);
                fn(err, response);
            }
        }); // end put


};


module.exports.getSession = function getSession(session_key, fn) {
    request.get({
        uri: couchdb.serverURL + '/' + dbName + '/' + session_key,
        json: true,
        headers: {"content-type": "application/json", 'User-Agent': 'vzw2.0'},
        time: true, timeout: 15000

    }, function (err, response) {
        if (err) {
            l.logger.error("getSession failed. Error: " + err.name + ': ' + err.message);
            return fn(err);
        }
        else if (response.statusCode == 200) {
            fn(null, response.body);
        } else {
            fn(null, null); // Session not found
        }

    });
};


// todo: generalize the following update functions

module.exports.updateConsent = function updateConsent(session_key, consent, fn) {

    var params = {
        TableName: tableName,
        Key: {
            "session_key": session_key
        },
        UpdateExpression: "set info.consent.form_response = :form_response",
        ExpressionAttributeValues: {

            ":form_response": consent.form_response
        },
        ReturnValues: "UPDATED_NEW"
    };

    l.logger.debug("Updating the consent item...parms = " + JSON.stringify(params, null, 2));
    docClient.update(params, function (err, data) {
        if (err) {
            l.logger.error("updateConsent : Unable to update item. Error: " + err.name + ': ' + err.message);
        } else {
            l.logger.debug("updateConsent UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
        fn(err);
    });


};

module.exports.updateConsumerInfo = function updateConsumerInfo(session_key, consumer_info, fn) {

    var params = {
        TableName: tableName,
        Key: {
            "session_key": session_key
        },
        UpdateExpression: "set info.phone_id.consumer_info = :consumer_info",
        ExpressionAttributeValues: {

            ":consumer_info": consumer_info
        },
        ReturnValues: "UPDATED_NEW"
    };

    l.logger.debug("updateConsumerInfo Updating the consumer_info item..." + JSON.stringify(params, null, 2));
    docClient.update(params, function (err, data) {
        if (err) {
            l.logger.error("updateConsumerInfo Unable to update item. Error: ", +err.name + ': ' + err.message);
        }
        else {
            l.logger.debug("updateConsumerInfo UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
        fn(err);
    });


};

module.exports.updateCart = function updateCart(session_key, cart, fn) {

    var params = {
        TableName: tableName,
        Key: {
            "session_key": session_key
        },
        UpdateExpression: "set info.cart = :cart",
        ExpressionAttributeValues: {

            ":cart": cart
        },
        ReturnValues: "UPDATED_NEW"
    };

    l.logger.debug("updateCart Updating the consumer_info item..." + JSON.stringify(params, null, 2));
    docClient.update(params, function (err, data) {
        if (err) {
            l.logger.error("updateCart Unable to update item. Error: ", +err.name + ': ' + err.message);
        }
        else {
            l.logger.debug("updateCart UpdateItem succeeded:", JSON.stringify(data, null, 2));
        }
        fn(err, data);
    });


};