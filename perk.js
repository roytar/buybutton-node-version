/**
 * Created by taranro on 5/9/17.
 */

/**
 *
 * Perks direct redemption
 */

'use strict';

const l = require('./helpers/logging');
var rp = require('request-promise');
var iperk = require('./iperk');
// var idb = require('./idb');
var db = require('./helpers/chbaseperk.js');

// main entry point usage: .../perk?id=<mdn>&campid=<perk campign id>
//
module.exports.perk = function perk(req, res, next) {

    l.logger.info('perk here id= ' + req.query.id + 'campid=' +  req.query.campid);
    // Get the campaign client id and secret.
    var campid = (!req.query.campid)?"32e74b96-2378-48c4-a477-5ef318e1273b" : req.query.campid;
    l.logger.info("campid= " + campid);
    db.openDB().then(r=> {
        db.getCampaign(campid)
            .then((r) => {
                // Get the access token
                var post_options = {
                    uri: iperk.auth_endpoint + '?' + 'grant_type=' + iperk.grant_type,
                    method: 'POST',
                    auth: {
                        user: r.client_id,
                        pass: r.client_secret
                    }
                };
                rp(post_options)
                    .then((response) => {
                        // l.logger.info('Access token: ' + JSON.parse(response).access_token);

                        var redeem_options = {
                            method: 'POST',
                            uri: iperk.redemption_endpoint + '/' + campid + '?' + 'mdn=' + req.query.id,
                            auth: {
                                bearer: JSON.parse(response).access_token
                            }
                        };
                        l.logger.info('redeeming....');
                        // now do the redeem
                        rp(redeem_options)
                            .then((response) => {

                                let jresp = JSON.parse(response);
                                jresp.type = 'Redeem';
                                jresp.campid = campid;
                                jresp.mdn = req.query.id;
                                jresp.statusCode = 200;
                                let json_response = JSON.stringify(jresp);
                                l.logger.info('redeem resp: ' + json_response);

                                db.putRedeemResult(jresp)
                                    .then((r) => {
                                        l.logger.info('put complete');
                                    }).catch((e) => {
                                    l.logger.error('put error')
                                });


                                res.setHeader('Content-Type', 'application/json');
                                res.send(json_response);
                            })
                            .catch((err) => {

                                let jresp = JSON.parse(err.error);
                                jresp.type = 'Redeem';
                                jresp.campid = campid;
                                jresp.mdn = req.query.id;
                                jresp.statusCode = err.response.statusCode;

                                let json_response = JSON.stringify(jresp);
                                l.logger.info('redeem failed: ' + json_response);

                                db.putRedeemResult(jresp)
                                    .then((r) => {
                                        l.logger.info('put complete(e)');
                                    }).catch((e) => {
                                    l.logger.error('put error(e)')
                                });


                                res.setHeader('Content-Type', 'application/json');
                                res.send(json_response);
                            });
                    })
                    .catch((err) => {
                        l.logger.error('error getting access token: ' + JSON.stringify(err.message));
                    });
            })
            .catch((err) => {
                l.logger.error('error getting campaign client id and secret: ' + err.message);
            });
    })
        .catch(err => {
            l.logger.error('open failed: ' + err.message);
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(err.message));
        });
};
