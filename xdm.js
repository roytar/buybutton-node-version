/**
 * Created by taranro on 5/9/17.
 */


'use strict';

const jsonwebtoken = require('jsonwebtoken');
const jwkToPem = require('jwk-to-pem');
const querystring = require('querystring');

const l = require('./helpers/logging');

var rp = require('request-promise');
var errors_rp = require('request-promise/errors');
var cdiauth = require('./cdiauth');

l.logger.info('mob Starting....');
l.logger.error('error logging on');
l.logger.warn('warning logging on');
l.logger.debug('debug logging on');

var uuid = require('node-uuid');

// main entry point usage: .../mob?cb=<callback url>&state=<state string>
//                          if no cb is provided, json will be sent.
//                          cb will be called with state and id=<mdn> if successful.
module.exports.mob = function mob(req, res, next) {

    var redirect = cdiauth.auth.auth_endpoint + '?' +
        querystring.stringify({
            'client_id': cdiauth.auth.client_id,
            'scope': cdiauth.auth.cdi_scope,
            'redirect_uri' : cdiauth.auth.redirect_uri + (req.query.cb?('?cb='+req.query.cb) : ''),
            'response_type' : cdiauth.auth.cdi_response_type,
            'state' : (req.query.state? req.query.state : uuid.v4())
        });

    l.logger.info('1.mob redirect: ' + redirect);
    res.redirect(redirect);
};

module.exports.cb = function cb(req, res, next) {

    // l.logger.info('\n+++mob cb here cdiauth = ' + JSON.stringify(req.query, null, 4));

    if (!req.query.code) {
        l.logger.error('mob/cb auth failed: ' + JSON.stringify(req.query, null, 4));
        jsonResponseOrRedirect(req, res, null, req.query);
    }
    else {
        doPostRequest(req.query.code, req.query.cb, (err, response) => {
            jsonResponseOrRedirect(req, res, err, response);
        });
    }
};

module.exports.dummycb = function dummycb(req, res, next) {
    l.logger.info('dummycb here: ' + JSON.stringify(req.query));
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(req.query));

};


// Utility functions:
// codesaver: if cb then redirect, otherwise send json back to the browser
function jsonResponseOrRedirect(req, res, err, json_response) {

    if (!req.query.cb) {
        res.setHeader('Content-Type', 'application/json');
        // statuscode 400
        // JSON.stringify(JSON.parse(json_response.error)

        l.logger.info('2. mob sending: ' + JSON.stringify(json_response));

        res.send(JSON.stringify(json_response));
    }
    else {

        let redirect_url = req.query.cb + '?state=' + req.query.state +
            (json_response.sub? ('&id=' + json_response.sub ) :
            '&'+ querystring.stringify(json_response));

        l.logger.info('2. mob redirect: ' + redirect_url);

        res.redirect(redirect_url);
    }
}


const jwk = {
    "keys": [
        {
            "alg": "RS256",
            "e": "AQAB",
            "n": "23zs5r8PQKpsKeoUd2Bjz3TJkUljWqMD8X98SaIb1LE7dCQzi9jwO58FGL0ieY1Dfnr9-g1iiY8sNzV-byawK98W9yFiopaghfoKtxXgUD8pi0fLPeWmAkntjn28Z_WZvvA265ELbBhphPXEJcFhdzUfgESHVuqFMEqp1pB-CP0",
            "kty": "RSA",
            "kid": "rsa1"
        }
    ]
};

var pub_key = jwkToPem(jwk.keys[0]);

// makes the post request and handles the jwt verify.
function doPostRequest(code, cb, callback) {
    var payload = querystring.stringify({'client_id':cdiauth.auth.client_id, 'client_secret':cdiauth.auth.client_secret,
        'code':code, 'redirect_uri':cdiauth.auth.redirect_uri + (cb?'?cb='+cb:''),
        'grant_type':cdiauth.auth.grant_type, 'scope':cdiauth.auth.cdi_scope, 'response_type':cdiauth.auth.cdi_response_type});



    // An object of options to indicate where to post to
    var post_options = {
        uri: cdiauth.auth.token_endpoint + '?' + payload,
        method: 'POST',
        resolveWithFullResponse: true
    };

    rp(post_options)
        .then((response)=> {
            // l.logger.info('doPostReq: ' + response.statusCode);

            var jsonBody = JSON.parse(response.body);
            // l.logger.info(JSON.stringify(jsonBody, null, 4));

            if (jsonBody.error===undefined) {

                try {
                    jsonwebtoken.verify(jsonBody.id_token, pub_key,
                        {
                            'algorithm': jwk.keys[0].alg,
                            'audience': cdiauth.auth.client_id,
                            'issuer': cdiauth.auth.token_issuer
                        },
                        function (err, response) {
                            // if token alg != RS256,  err == invalid signature
                            if (!err) {  // good response!
                                callback(null, response);
                            }
                            else {
                                l.logger.error('Token verify error: ' + err + JSON.stringify(response));
                                callback(err, {'errorType': 'token Verify'});
                            }
                        });
                }
                catch (err) {
                    l.logger.error('mob/cb webtoken verify error: ', err);
                    callback(err, {'errorType': 'token Verify'});
                }
            }
            else {
                l.logger.error('mob/cb received error: ' + JSON.stringify(jsonBody));
                callback(jsonBody.error, JSON.stringify(jsonBody));
            }
        }).catch(errors_rp.StatusCodeError, (reason) => {

        // token request failed (response error)
        l.logger.error('mob/cb get token failed (response error): statusCode=' +
            reason.statusCode + ' error=' + reason.error.error +': ' + reason.error.error_description);

        callback(reason.statusCode, {'errorType': 'Response from token request', 'statusCode': reason.statusCode,
            'statusMessage': reason.response.statusMessage,
            'token endpoint': reason.response.request.href.substr(0,reason.response.request.href.indexOf('?'))});

        // callback(reason.statusCode, reason);

    }).catch(errors_rp.RequestError, (reason) => {
        //  get token request failed (request error)
        l.logger.error('mob/cb get access token failed (request error): cause= ' +
            reason.cause.code + ' error= ' + reason.error.errno +' error message= ' + reason.error.message, ' host= ', reason.error.host);

        callback(reason.statusCode, {'errorType': 'Request', 'statusCode': reason.cause.code,
            'Error Message': reason.error.message, 'host': reason.error.host});
    });
}









