// const express = require('express');
// var router = express.Router();
'use strict';

const querystring = require('querystring');

const danal = require('./helpers/danal');
const cartmgr = require('./helpers/cartmgr');
const datamodel = require('./helpers/datamodel');
const session = require('./helpers/dbsession');
const asyncmgr = require('./helpers/asyncmgr');
const l = require('./helpers/logging');

l.logger.info('Starting....');
l.logger.error('error logging on');
l.logger.warn('warning logging on');
l.logger.debug('debug logging on');

var failed_data = {
    "ConsumerAddress1": "485 MADison Avenue",
    "ConsumerEmail": "anewman@madmag.com",
    "ConsumerAddress2": "",
    "ConsumerPreferredContact": "",
    "Method": "GetConsumerInfoRes",
    "ConsumerFirstName": "Alfred",
    "ConsumerDOB": "",
    "CorrelationID": "2703c90005d211e7ae58b77280841b4b",
    "ConsumerPostalCode": "10022",
    "ConsumerCity": "Metuchen",
    "ConsumerState": "NJ",
    "ConsumerLastName": "Newman",
    "Version": "1.0.0",
    "ErrorCode": "0",
    "ReferenceID": "20170310204301M166CG79163",
    "ConsumerCountryCode": "US"
};


module.exports.af = function af(req, res, next) {


    var session_key = session.getNewSessionKey();
    var client_ip = req.headers['x-forwarded-for'];
    var my_redir_url = req.protocol + '://' + req.get('host') + '/afcb' + '?' + 'sid=' + session_key;


    var d = new Date();
    var epoc_time = d.getTime();

    danal.startPhoneID(my_redir_url, function (error, parsedResponse) {
        if (error === null) {
            // add session_key,phone_id_time and akey to db
            var phone_id = new datamodel.PhoneID(parsedResponse.AuthenticationKey, epoc_time, req.headers, req.query,
                parsedResponse);

            session.createSession(session_key, {
                'akey': parsedResponse.AuthenticationKey,
                'startTime': epoc_time
            }, function (err) {

                if (err === null) {
                    l.logger.info("db success, redirecting to " + parsedResponse.EVURL);
                    res.redirect(parsedResponse.EVURL);
                }
                else {

                    l.logger.info('db failed!!!');


                    res.status(500).send('Error db error createSession failed!');
                }
            });


        }
        else {

            l.logger.info('af: phoneid failed (immediate)!' + 'Error: ' + error.name + ': ' + error.message);

            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({'first_name': 'FJohn', 'last_name': 'FPublic'}));
        }

    });


    // res.render('index', {title: 'ebb - good'});

};


// Callback function from danal redirect
module.exports.afcb = function afcb(req, res, next) {
    var cart, phone_id, consent;

    if (req.query && req.query.sid == undefined) {
        // he didn't send in the session id


        //;;;;;;;;;;;;;;;;;;;;;;;;;;;

        res.status(400).send('Bad Request');
        return;
    }

    // get the session
    session.getSession(req.query.sid, function (err, data) {
        const host_url = req.protocol + '://' + req.get('host');
        if (req.query.ErrorCode == '0') {
            l.logger.info('Phoneid success!');


            if (!err && data.akey) {
                // phone id success and we found the session
                l.logger.info('afcb: phone id success akey=' + data.akey);

                var consent = new datamodel.Consent('dummy/redirect'); // setup dummy consent
                consent.form_response.zipcode = '90869';
                consent.form_response.consentid = '123456789';
                // consent.form_response.consent_permission = form_response.consent_permission||' ';
                consent.form_response.date = new Date();
                // consent.form_response.ppUrl = form_response.pp_Url;
                // consent.form_response.tc_url = form_response.tc_url;
                consent.form_response.response = 'dummyed';

                danal.getConsumerInfo(data.akey, consent,
                    function (err, response) {
                        if (!err) {
                            l.logger.debug('gci response = ' + JSON.stringify(response));

                            res.setHeader('Content-Type', 'application/json');
                            delete response['EncryptionKey'];
                            delete response['Method'];
                            res.send(JSON.stringify({response}));

                        }
                        else {

                            l.logger.error('gci Error: ' + err.name + ': ' + err.message);
                            res.setHeader('Content-Type', 'application/json');
                            res.send(JSON.stringify({'first_name': 'FJohn', 'last_name': 'FPublic'}));


                        }


                    }, 'AR');


            }
            else {
                l.logger.info('PhoneID: success getSession but no session!!!: ' + err);


                res.status(400).send('Bad Request');

            }
        }
        else {
            // Phone id failed

            if (!err) {
                // We found the session
                l.logger.info('Phone ID failed...');

                res.setHeader('Content-Type', 'application/json');


                res.send(JSON.stringify({'response': failed_data}));


            }
            else {
                // Phone id failed and we didn't find the session
                l.logger.warn('Phone ID failed and no session, must be a spoof!');
                res.status(400).send('Bad Request');


            }
        }
    });
};

module.exports.getPhoneAttr = function getPhoneAttr(req, res, next) {

    danal.getPhoneAttributes(req.query.mdn, function (error, parsedResponse) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.parse(JSON.stringify(parsedResponse)));

    });
}