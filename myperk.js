/**
 * Created by taranro on 2/20/17.
 */


/*

    myperks.js - experimental module used for trying mobile redemption and expanded scope.
    perk.js is the production code.

 */

'use strict';

const l = require('./helpers/logging');
// var request = require('request');

var rp = require('request-promise');
var errors_rp = require('request-promise/errors');




module.exports.do_xperk = function do_xperk(req, res, next) {

    const response_type = 'code';

    var session_key = session.getNewSessionKey();

    // var my_redir_url = req.protocol + '://' + req.get('host') + '/perk/callback';
    var my_redir_url = 'https://bbdev2.vmcomlab.com/perk/callback';


    // var scope = 'openid+mobileperks';

    var redirect = authorize_endpoint + '?' + 'client_id=' + client_id +
        '&scope=' + encodeURIComponent(scope) +
        '&redirect_uri=' + encodeURIComponent(my_redir_url) +
        '&response_type=' + response_type +
        '&state=' + session_key +
        '&campaign_id=' + campaign_id;


    l.logger.info('do_perk: 1. redirect = ' + redirect);

    res.redirect(redirect);

};


module.exports.do_xperkcallback = function do_xperkcallback(req, res, next) {


    if (req.query.error) {
        l.logger.info('do_perkcallback: 1***** auth failed: error= ' + req.query.error + 'error_description='+req.query.error_description);
        // res.render('index', {title: req.query.error + ': ' +req.query.error_description});

        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({'error': req.query.error, 'error_description': req.query.error_description}));

    }
    else {


        const token_endpoint = 'https://auth.svcs.verizon.com/vzconnect/token';

        var grant_type = 'authorization_code';
        var nonce = '12345';

        // var scope = 'openid mobileperks phone';
        // var scope = 'mobileperks%20openid%20phone';
        var scope = 'mobileperks%20openid';

        var my_redir_url = 'https://bbdev2.vmcomlab.com/perk/callback';


        var access_token_uri = token_endpoint + '?' +
            'client_id=' + client_id +
            '&client_secret=' + client_secret +
            '&code=' + req.query.code +
            '&scope=' + scope +
            '&redirect_uri=' + encodeURIComponent(my_redir_url) +
            '&grant_type=' + grant_type +
            '&nonce=' + nonce;

        var access_token_req_options = {
            method: 'POST',
            uri: access_token_uri,
            resolveWithFullResponse: true,
            // simple: false,
            json: true
        };


        l.logger.info('do_perkcallback: 2. Post for access token= ' + access_token_uri);

        rp(access_token_req_options)
            .then((response)=>{
                l.logger.info('do_perkcallback 2 POST Response: ' +
                    response.statusCode + ' error= ' + response.body.error + ': ' + response.body.error_description);

                var access_token = response.body.access_token;

                l.logger.info("do_perkcallback: access token= " + access_token);

                var userinfo_uri = 'https://api.yourmobileid.com/userinfo';

                var userinfo_req_options = {
                    method: 'GET',
                    uri: userinfo_uri,
                    headers: {
                        'Authorization': 'Bearer '+ access_token
                    },
                    resolveWithFullResponse: true,
                    json: true
                };

                var redeem_uri = 'https://perks.svcs.verizon.com/mobileperks/redeem';

                var redeem_req_options = {
                    method: 'POST',
                    uri: redeem_uri,
                    headers: {
                        'Authorization': 'Bearer '+ access_token
                    },
                    resolveWithFullResponse: true,
                    json: true
                };



                rp(redeem_req_options)
                    .then((response)=>{

                    l.logger.info('do_perkcallback redeem: statusCode=' +
                            response.statusCode +
                            'entity= ' + JSON.stringify(response.body.entity));

                        // res.render('index', {title: JSON.stringify(response.body.entity)});

                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify(response.body.entity));

                    }).catch(errors_rp.StatusCodeError, (reason) => {

                    // Redeem failed (response error)

                        l.logger.info('do_perkcallback get redeem (response error): statusCode=' +
                            reason.statusCode + ' error=' + reason.error.error +': ' + reason.error.error_description);

                        l.logger.info('do_perkcallback get userinfo (response error): reason.message= ' + reason.message);

                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify({'error': reason.error.error, 'error_description': reason.error.error_description}));



                    }).catch(errors_rp.RequestError, (reason) => {

                    // Redeem request failed (request error)
                        l.logger.info('do_perkcallback get redeem failed (request error): cause= ' +
                            reason.cause.code + ' error= ' + reason.error.errno +' error message= ' + reason.error.message, ' host= ', reason.error.host);

                        res.setHeader('Content-Type', 'application/json');
                        res.send(JSON.stringify({'type': 'request (redeem error)', 'cause': reason.cause.code, 'error': reason.error.errno, 'error_message': reason.error.message}));
                })

            }).catch(errors_rp.StatusCodeError, (reason) => {

            // access token request failed (response error)
                l.logger.info('do_perkcallback get access token failed (response error): statusCode=' +
                    reason.statusCode + ' error=' + reason.error.error +': ' + reason.error.error_description);

                l.logger.info('do_perkcallback get access token failed (response error): reason.message= ' + reason.message);
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({'error': reason.error.error, 'error_description': reason.error.error_description}));



        }).catch(errors_rp.RequestError, (reason) => {
            // access token request failed (request error)
                l.logger.info('do_perkcallback get access token failed (request error): cause= ' +
                    reason.cause.code + ' error= ' + reason.error.errno +' error message= ' + reason.error.message, ' host= ', reason.error.host);
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({'type': 'request error (access token req)', 'cause': reason.cause.code, 'error': reason.error.errno, 'error_message': reason.error.message}));

        });

    }
};

